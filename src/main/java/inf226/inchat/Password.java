package inf226.inchat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Password {

    private final String keyWord;

    public Password(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getPassword(){
        return keyWord;
    }

    public int getLength(){
        return this.getPassword().length();
    }

    /* Seeing if the password has
    * at least 1 lowercase letter
    * at least 1 uppercase letter
    * 1 digit or special letter,
    * and be between 8-to-20 letters long.
    * */
    public boolean checkPassword(){
        String checker = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20}$";
        Pattern pattern = Pattern.compile(checker);
        Matcher matcher = pattern.matcher(this.getPassword());
        return matcher.matches();
    }

}
