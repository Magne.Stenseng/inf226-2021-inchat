package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;

    public EventStorage(Connection connection) throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event) throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        
        String sql =  "INSERT INTO Event VALUES('" + stored.identity + "','"
                                                  + stored.version  + "','"
                                                  + event.channel + "','"
                                                  + event.type.code + "','"
                                                  + event.time  + "')";
        connection.createStatement().executeUpdate(sql);
        switch (event.type) {
            case message:
                sql = "INSERT INTO Message VALUES('" + stored.identity + "','"
                                                     + event.sender + "','"
                                                     + event.message +"')";
                break;
            case join:
                sql = "INSERT INTO Joined VALUES('" + stored.identity + "','"
                                                  + event.sender +"')";
                break;
        }
        connection.createStatement().executeUpdate(sql);
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event, Channel.Event new_event)
            throws UpdatedException, DeletedException, SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        String sql = "UPDATE Event SET" +
            " (version,channel,time,type) =('" 
                            + updated.version  + "','"
                            + new_event.channel  + "','"
                            + new_event.time  + "','"
                            + new_event.type.code
                            + "') WHERE id='"+ updated.identity + "'";
        connection.createStatement().executeUpdate(sql);
        switch (new_event.type) {
            case message:
                sql = "UPDATE Message SET (sender,content)=('" + new_event.sender + "','"
                                                     + new_event.message +"') WHERE id='"+ updated.identity + "'";
                break;
            case join:
                sql = "UPDATE Joined SET (sender)=('" + new_event.sender +"') WHERE id='"+ updated.identity + "'";
                break;
        }
        connection.createStatement().executeUpdate(sql);
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
            throws UpdatedException, DeletedException, SQLException {

        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            String sql =  "DELETE FROM Event WHERE id ='" + event.identity + "'";
           connection.createStatement().executeUpdate(sql);
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id) throws DeletedException, SQLException {
        final String sqlQuery = "SELECT version,channel,time,type FROM Event WHERE id = ? ";
        final String sql = id.toString();
        final PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setString(1, sql);
        final ResultSet rs = statement.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            final PreparedStatement mstatement;
            switch(type) {
                case message:
                    final String msqlQuery = "SELECT sender,content FROM Message WHERE id = ? ";
                    final String msql = id.toString();
                    mstatement = connection.prepareStatement(msqlQuery);
                    mstatement.setString(1, msql);
                    final ResultSet mrs = mstatement.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    final String asqlQuery = "SELECT sender FROM Joined WHERE id = ? ";
                    final String asql = id.toString();
                    mstatement = connection.prepareStatement(asqlQuery);
                    mstatement.setString(1, asql);
                    final ResultSet ars = mstatement.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
