package inf226.inchat;

public final class UserName {
    private final String name;

    public UserName(String name){
        this.name = name;
    }

    public String getUserName() {
        return name;
    }
}
